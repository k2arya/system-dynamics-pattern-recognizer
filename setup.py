from setuptools import setup, find_packages
from sdpr import __version__

setup(name='sdpr',
      version=__version__,
      description='System Dynamics Pattern Recognizer',
      author='Korhan Kanar',
      packages=find_packages(include=['sdpr', 'sdpr.*']),
      package_data={'sdpr': ['mod/models.json', 'img/patterns.png']}
      )
